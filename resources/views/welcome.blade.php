@extends('layouts.app')

@section('content')

@if (session('access.denied.revisor.only'))
<div class="alert alert-danger text-center">
    {{ __('ui.accessDenied') }}
</div>
@endif




         
<div class="masthead">
    <div class="container-fluid h-100">
        <div class="row h-100 d-flex align-items-center justify-content-center">
            
            <div class="col-12 col-md-6 text-md-center">

                <div class="card card-custom shadow-lg p-5 m-5">

                    <h2 class="pt-md-3 pb-md-3 font-weight-bold">{{ __('ui.search') }}</h2>

                    <form action=" {{route('search')}} " method="GET" class="mt-1">

                        <div class="row d-flex justify-content-center">
                            
                            <div class="col-12 col-md-10 py-3 p-md-0">
                            <input class="input-search form-control pl-md-3 mr-md-1" type="text" name="q">
                            </div>

                            <div class="col-12 col-md-2">
                            <span>
                                <button class="btn btn-search" type="submit">
                                    <span style="color:white;">
                                        <i class="fas fa-search"></i>
                                    </span>
                                </button>
                            </span>
                            </div>
                            

                            
                        </div>




                    </form>
                </div>


            </div>
        </div>
    </div>
</div>





<div class="container">
    <div class="row">

        @foreach ($announcements as $announcement)

            @include('includes._announcement')

        @endforeach

    </div>
</div>






@endsection
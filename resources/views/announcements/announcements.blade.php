@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-12">
            <h1>{{ __('ui.announcementForCategory')}} {{$category->name}}</h1>
        </div>
    </div>

    <div class="row">

        @foreach ($announcements as $announcement)

            @include('includes._announcement')

        @endforeach

    </div>

    <div class="row justify-content-center">
        <div class="col-12">
            {{$announcements->links()}}
        </div>
    </div>

</div>

@endsection
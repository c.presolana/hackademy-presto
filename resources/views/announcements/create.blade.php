@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class=col-12>
        <form action="{{route('announcements.store')}}" method="POST" enctype="multipart/form-data">
          @csrf
                            <div class="form-group">                 
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" name="email" value="{{old('email')}}"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                            <label for="name">Nome</label>
                            <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" aria-describedby="name">
                            </div>
                            <div class="form-group">
                            <label for="phone">Telefono</label>
                            <input type="text" name="phone"value="{{old('phone')}}" class="form-control" id="phone" aria-describedby="phone">
                            </div>
                            <div class="form-group">
                            <label for="message">Messaggio</label>
                            <textarea type="text" id="message" class="form-control" rows="3" name="message" value="{{old('message')}}" aria-describedby="scrivici">
                            </textarea>
                            </div>


                            <button type="submit" class="btn btn-primary">Invia</button>
                        </form>

        </div>
    </div>
</div>
@endsection
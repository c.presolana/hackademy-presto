@extends('layouts.app')

@section('content')

<div class="container ">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-md-6">
            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            <div class="row my-3 py-3 d-flex justify-content-center">
                <h2>{{ __('ui.createYourAnnouncement')}}</h2>
            </div>

            <div class="card">

                <div class="card-body">
                     <p style="display:none">{{$uniqueSecret}}</p>
                    <form action="{{ route('announcement.create') }}" method="POST">
                        @csrf

                        <div class="form-group pb-1">
                            <label for="title" class="font-weight-bold">{{ __('ui.title') }}</label>
                            <input value="{{ old('title') }}" name="title" type="string" class="form-control" id="" aria-describedby="titleHelp">
                        </div>


                        <div class="form-group pb-1">
                            <label for="price" class="font-weight-bold">{{ __('ui.price') }}</label>
                            <input value="{{ old('price') }}" name="price" type="number" class="form-control" id="" aria-describedby="priceHelp" placeholder="€">
                        </div>


                        <div class="input-group">

                            <div class="input-group-prepend pb-3">
                                <label style="color: black" class="input-group-text font-weight-bold" for="category">{{ __('ui.category') }}</label>
                            </div>

                            <select class="custom-select" name="category_id" id="category">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{old('category') == $category->id ? 'selected' : ''}}>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>

                        </div>


                        <div class="form-group">
                            <label for="body" class="font-weight-bold">{{ __('ui.announcement')}}</label>
                            <textarea name="body" id="body" rows="3" placeholder="Descrivi il tuo prodotto" class="form-control">{{old('body')}}</textarea>
                        </div>

                        
                            <div class="col-12">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        

                        <div class="form-group row">
                            <label class="col-md-12 col-form-label text-md-left">
                                 {{ __('ui.images')}}  
                            </label>
                            <div class="col-md-12">
                                <div class="dropzone" id="drophere"></div>
                            </div>                   
                        </div>

                        @error('images')
                        <span class="invalid-feedback" role="alert"><strong> {{$message}}
                        </strong></span>
                        @enderror

                        <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
                        
                        <button type="submit" class="btn btn-lg btn-confirm mt-2">{{ __('ui.create') }}</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@extends('layouts.app')

@section('content')

<section class="content-section" id="portfolio">
    <div class="container">
        <div class="content-section-heading text-center">
            <h1 class="mb-0">{{ __('ui.searchFor')}} {{ $q }}</h1>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">


        @foreach ($announcements as $announcement)

            @include('includes._announcement')

        @endforeach

    </div>

</div>

@endsection
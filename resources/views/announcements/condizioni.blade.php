@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row"> 
        <div class="col-12">
            <h1>Condizioni generali del servizio</h1> 

            <div class="col-12 d-flewx align-items-center">

                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="flase" aria-controls="collapseOne">
                            Contenuto del servizio
                            </button>
                        </h2>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                        Presto.it S.r.l. società di servizi informatici e telematici con sede legale in Via della Liberazione- Roma, Italia, mette a disposizione degli utenti che intendano avvalersene un servizio (da ora in poi il "Servizio Base" e/o "Servizio") web based che consente di pubblicare e consultare annunci e inserzioni di soggetti privati o professionali che intendano alienare o acquistare beni o prestare e ricevere servizi. Il Servizio consente altresì agli utenti inserzionisti ed agli utenti interessati a quanto pubblicato di entrare in contatto tra di loro.


                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Titolarità della piattaforma
                            </button>
                        </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                        Presto.it S.r.l., è l'unica titolare della piattaforma web per il tramite della quale viene gestito il Servizio nonchè di tutti i relativi diritti inerenti e conseguenti allo sfruttamento della piattaforma medesima.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Applicabilità delle condizioni
                            </button>
                        </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                        Le presenti Condizioni Generali del Servizio si applicano sia agli utenti che utilizzino il Servizio in consultazione degli annunci pubblicati sia agli utenti inserzionisti privati o professionali (d‘ora in poi collettivamente “utente/i”).   
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Termini per l'uso del servizio
                            </button>
                        </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                        L’utilizzo del Servizio è consentito solo ad utenti maggiorenni secondo la legge italiana (maggiori di anni 18). L’eventuale utilizzo del Servizio è concesso altresì agli utenti che abbiano compiuto sedici anni  esclusivamente a valle della  autorizzazione e comunque sotto la supervisione dei genitori o di chi ne esercita la potestà o la tutela, che assumeranno quindi ogni responsabilità dell’operato del minore nei confronti di Presto.it S.r.l. e dei terzi ad ogni titolo coinvolti.

                        L'utilizzo del Servizio Base consente la libera consultazione degli annunci e la pubblicazione di inserzioni e la creazione di utenze finalizzate all'utilizzo del servizio medesimo. Talune funzionalità specifiche del Servizio, l'inserzione in specifiche categorie merceologiche e le inserzioni ulteriori rispetto alle soglie fissate per talune categorie potranno essere messe a disposizione solamente a pagamento così come da specifiche condizioni di contratto relative ai servizi medesimi.

                        Le relazioni intrattenute tra gli utenti del Servizio, incluso l'acquisto, lo scambio di informazioni, anche per il tramite del form di risposta all’annuncio, la consegna o il pagamento di beni o servizi, avvengono esclusivamente tra utenti senza che Presto.it S.r.l. sia parte della relazione. L'utenza si impegna, altresì, a non fare utilizzo improprio dei contatti presenti a qualunque titolo sulla piattaforma di Presto.it. A titolo esemplificativo ma non esaustivo è vietato l'invio di pubblicità, materiale promozionale, o qualsiasi altra forma di sollecitazione non autorizzata o non richiesta tramite e-mail o con qualsiasi altro metodo di contatto.

                        Gli utenti manterranno la titolarità di tutti diritti di proprietà relativi a video, fotografie, fermo immagini, audio o altro materiale (di seguito, le “Riproduzioni”) che Presto.it ha consentito agli stessi di pubblicare sul proprio sito. Fermo restando quanto precede, l’utente concede a Presto.it una licenza non esclusiva, gratuita, perpetua, trasferibile, irrevocabile e senza limiti di territorio, per utilizzare (anche in combinazione con altri materiali audio e/o video e/o fotografici) e, dunque, fissare, all’interno del sito e/o su ogni altro mezzo (sia esso digitale, elettronico o cartaceo), riprodurre, modificare, adattare, tradurre, distribuire, pubblicare, visualizzare, riprodurre in pubblico le Riproduzioni dell’utente, impegnandosi peraltro a non opporsi alla pubblicazione, utilizzo, modifica, cancellazione e sfruttamento delle Riproduzioni da parte di Subito.it.

                        Con l’utilizzo del Servizio, gli utenti si assumono l’esclusiva responsabilità circa il diritto di utilizzo delle Riproduzioni pubblicate dagli stessi sul sito, manlevando e tenendo indenne Subito.it da qualsiasi pretesa e/o richiesta di risarcimento formulata da soggetti terzi a fronte della violazione dei diritti di quest’ultimi.

                        Al momento della registrazione sul sito da parte dell’utente, quest’ultimo sarà tenuto a fornire i propri dati anagrafici reali senza ricorrere all'utilizzo di indirizzi email temporanei o alias (ovvero indirizzi associati al proprio indirizzo email dal quale però non è possibile inviare email ma solo riceverle). Resta espressamente inteso che, in caso di mancata osservanza delle disposizioni della presente clausola da parte dell’utente, Subito.it sarà considerata liberata dell’onere di fornire taluni servizi di assistenza allo stesso, essendo questa impossibilitata alla verifica della corrispondenza utente-email.       
                        </div>
                        </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingFive">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Responsabilità dell'utente
                            </button>
                        </h2>
                        </div>
                      <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                        L'utente è totalmente ed esclusivamente responsabile dell'uso del Servizio (da intendersi espressamente con riguardo alle funzioni di pubblicazione, di consultazione, di gestione delle inserzioni e di contatto tra utenti) ed è pertanto l'unico garante e responsabile dei beni e dei servizi offerti per il tramite del Servizio nonchè della correttezza, completezza e liceità delle inserzioni e del proprio comportamento nell'ambito del contatto tra utenti. L'utente garantisce la disponibilità e/o la titolarità del bene/servizio oggetto delle inserzioni medesime. L'utente garantisce altresì che i propri annunci non violano alcun diritto d'autore né diritto di proprietà industriale né altro diritto di terzi. In caso di contestazione da parte di terzi riguardo a qualsiasi annuncio o condotta ad esso legata, l'utente se ne assume la piena responsabilità e si impegna a tenere manlevata e indenne Subito.it S.r.l. da qualsiasi danno, perdita o spesa. L'utente si impegna ad utilizzare il form di risposta all’annuncio al solo scopo di prendere contatto e scambiare informazioni con gli altri utenti relativamente agli annunci, utilizzando un linguaggio consono, nel rispetto della legge, dell’etica e della netiquette. L'utente, inoltre, si assume ogni responsabilità per eventuali danni che possano derivare al suo sistema informatico dall'uso del Servizio o a quello di terzi. Resta infine inteso che ogni eventuale utilizzo di robot, spider, scraper e/o ulteriori  strumenti automatici per accedere al sito e/o per estrapolare i relativi dati, contenuti, informazioni è espressamente vietato.
                        </div>
                       </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Limitazioni della responsabilità
                            </button>
                        </h2>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                        <div class="card-body">
                        Presto.it S.r.l. non presta alcuna garanzia circa il contenuto, la completezza e la correttezza delle inserzioni pubblicate nè con riguardo ai dati pubblicati, né relativamente alle informazioni successivamente fornite dall’utente, nè con riferimento al numero o alla qualità dei risultati ottenuti tramite il Servizio. In ogni caso Presto.it S.r.l. si riserva, in qualsiasi momento, il diritto di valutare, approvare, eliminare o impedire l'inserzione ovvero il diritto di inibire la consultazione o il contatto per il tramite del form di risposta all’annuncio nel caso in cui, a proprio insindacabile giudizio, l'uso del Servizio da parte dell'utente si riferisca a particolari sezioni merceologiche o possa considerarsi lesivo di diritti o delle prerogative di Presto.it S.r.l. o di terzi. Resta inteso che anche in caso di valutazione ed approvazione delle inserzioni (siano esse automatiche o manuali) Presto.it S.r.l. non presta alcuna garanzia circa il contenuto, la completezza e la correttezza delle stesse. Subito.it S.r.l. è altresì estranea alle trattative eventualmente nascenti dall'uso del Servizio e pertanto non garantisce nè la bontà nè l'esito delle stesse, di conseguenza nessuna richiesta di restituzione, compensazione, riparazione e/o risarcimento a qualunque titolo potrà essere indirizzata nei confronti di Presto.it S.r.l.

                        Il Servizio è offerto per il tramite del sito www.presto.it, del m-site m.subito.it e delle applicazioni mobile che possono contenere banner/link ad altri siti Internet o applicazioni che non sono sotto il controllo di Presto.it S.r.l.; la pubblicazione dei predetti banner/link non comporta l’approvazione o l’avallo da parte di Subito.it S.r.l. dei relativi siti e dei loro contenuti, né implica alcuna forma di garanzia da parte di quest’ultima che pertanto non si assume alcuna responsabilità. L'utente riconosce, quindi, che Presto.it S.r.l. non è responsabile, a titolo meramente esemplificativo, della veridicità, correttezza, completezza, del rispetto dei diritti di proprietà intellettuale e/o industriale, né risponde della loro eventuale contrarietà all’ordine pubblico, al buon costume e/o alla morale.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSeven">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            Giurisdizione
                            </button>
                        </h2>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                        <div class="card-body">
                        Presto.it S.r.l.e gli utenti sono regolati dalla legge e dalla giurisdizione italiana, in base alla quale anche le presenti Condizioni Generali dovranno essere interpretate. Salvo quanto disposto da norme di legge non derogabili, il Tribunale di Romasarà competente in via esclusiva a dirimere ogni controversia riguardante le presenti Condizioni Generali ed i rapporti dalle stesse regolati.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingEight">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            Validità delle condizioni generali
                            </button>
                        </h2>
                        </div>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                        <div class="card-body">
                        Le presenti condizioni Generali di Servizio si considereranno applicabili, ove compatibili, anche in caso di ulteriori, diversi e specifici accordi relativi ai servizi a pagamento (ed in particolare con riferimento ai servizi "Premium", "Pro" e "Promuovi"). Presto.it S.r.l. potrà comunque apportare unilateralmente in qualsiasi momento modifiche alle presenti Condizioni Generali dandone comunicazione sulla propria piattaforma web.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingNine">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                            Modifiche
                            </button>
                        </h2>
                        </div>
                        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                        <div class="card-body">
                        Le condizioni potrebbero essere soggette a modifiche. In caso di sostanziali modifiche, Presto.it S.r.l. avviserà l’utente pubblicandole con la massima evidenza sulle proprie pagine o tramite email o altro mezzo di comunicazione.
                        </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>    
@endsection
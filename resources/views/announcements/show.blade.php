@extends('layouts.app')

@section('content')

<div class="container mt-5">
    <div class="row">

        <div class="col-12 col-md-7">

            <div id="show-slider" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($announcement->images as $key => $image)
                    <li data-target="#show-slider" data-slide-to="{{$key}}" class="active">
                    </li>
                    @endforeach
                </ol>

                <div class="carousel-inner">

                    @foreach($announcement->images as $key => $image)
                    <div class="carousel-item {{$key == 0 ? 'active' : '' }}">
                        <img class="d-block w-100 rounded" src="{{ $image->getUrl(600,300) }}" alt="First slide">
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        @php
            $user = $announcement->user->id;
        @endphp
        <div class="col-12 col-md-5">
            <div class="card-body">
                <h3 class="card-title pb-0 mb-0">{{$announcement->title}}</h3>
                <p class="card-text"><small class="text-muted">{{$announcement->created_at->format('d/m/y')}} - {{$announcement->user->name}}</small>
                <a class="btn btn-light" href="{{route('announcements.usershow', compact('user'))}}" role="button">{{ __('ui.show')}}</a></td></p>
                <h3 style="color: #f9423a" class="card-text font-weight-bold">{{$announcement->price}} €</h3>
                <p class="card-text">{{$announcement->body}}</p>
                <a href="{{route('announcements.create')}}" button type="submit" class="btn btn-lg btn-confirm">{{ __('ui.contact')}}</a>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-12">
            <h1>{{ __('ui.relatedAnnouncements')}}</h1>

            <div class="row">
                @foreach ($announcements as $announcement)
                    @include('includes._announcement')
                @endforeach
            </div>

        </div>
    </div>
</div>

@endsection
@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row"> 
        <div class="col-12 d-flex align-items-center">
            <h1>Privacy</h1> 
        </div>
        <div class="col-12 d-flewx align-items-center">
         <p>Informativa ai sensi del Regolamento UE 2016/679 (“GDPR”)</p>
        </div>
        <div class="col-12 d-flewx align-items-center">

                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="flase" aria-controls="collapseOne">
                            Quali tipi di dati raccogliamo?
                            </button>
                        </h2>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                        Quando usi i nostri servizi, accetti che la nostra azienda raccolga alcuni tuoi dati personali. Questa pagina ha lo scopo di dirti quali dati raccogliamo, perché e come li usiamo.

                            <ul> 
                                <h5>Due tipi di dati:</h5>
                            <li>Dati forniti dall’utente</li>
                            <li>Che raccogliamo automaticamente</li>
                            </ul> 
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Come utilizziamo i dati raccolti
                            </button>
                        </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                        Utilizziamo i dati raccolti per offrirti ogni giorno il nostro servizio, per informarti sulle nostre attività commerciali o per proporti un servizio più personalizzato ed in linea con i tuoi interessi.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Il conferimento dei dati è obbligatorio
                            </button>
                        </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                        Il conferimento dei dati personali è obbligatorio esclusivamente per i trattamenti necessari all’erogazione dei servizi offerti da Presto (l’eventuale rifiuto per finalità di erogazione del servizio rende impossibile l’utilizzo del servizio stesso); è invece facoltativo per le finalità promozionali e di profilazione e l’eventuale rifiuto di prestare il consenso non ha conseguenze negative sull’erogazione del servizio offerto nell’ambito del sito web www.Presto.it e relative applicazioni.
                            
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Come e per quanto tempo i tuoi dati saranno conservati?
                            </button>
                        </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                                La conservazione dei dati personali avverrà in forma cartacea e/o elettronica/informatica e per il tempo strettamente necessario al soddisfacimento delle finalità di cui al punto 1, nel rispetto della tua privacy e delle normative vigenti.

                                Se non eserciti alcuna azione attiva (ad esempio navigazione, ricerche e/o ogni altra modalità di utilizzo del servizio) su Subito.it per un periodo di 27 mesi, verrai classificato come utente inattivo e i tuoi dati personali saranno cancellati automaticamente dalla piattaforma.

                                Per finalità di marketing diretto e profilazione trattiamo i tuoi dati, secondo il consenso che hai fornito, per un periodo massimo pari a quello previsto dalla normativa applicabile (rispettivamente pari a 24 e 12 mesi).

                                Per finalità di sviluppo e miglioramento del servizio e relative funzionalità, i dati personali dell’utente attivo potranno essere analizzati e conservati in forma aggregata e pseudoanonimizzata fino a 36 mesi. Tali dati sono riservati e ad esclusivo uso interno.

                                Per le medesime finalità ed una maggiore sicurezza del servizio mediante attività di moderazione e prevenzione di condotte contrarie alle condizioni e alle regole editoriali di Presto – anche in collaborazione con le Autorità – gli annunci, i contenuti generati dall’utente sulla piattaforma e quelli relativi alla sua identificazione (ad eccezione delle richieste di assistenza nei confronti del Servizio Clienti, la cui conservazione segue il ciclo di vita dell’utente) saranno in ogni caso conservati dal titolare, in forma protetta e con accesso limitato, per un periodo pari a 36 mesi. Tali contenuti riservati saranno ad esclusivo uso interno e potranno essere forniti dal titolare solo all’Autorità Giudiziaria o al difensore dell’utente per finalità di giustizia.

                                Le fatture, i documenti contabili e i dati relativi alle transazioni sono conservati per 11 anni ai sensi di legge (ivi compresi gli obblighi fiscali).

                                Nel caso di esercizio del diritto all’oblio attraverso richiesta di cancellazione espressa dei dati personali trattati dal titolare, ti ricordiamo che tali dati saranno conservati, in forma protetta e con accesso limitato, unicamente per finalità di accertamento e repressione dei reati, per un periodo non superiore ai 12 mesi dalla data della richiesta (salvo quanto sopra indicato in merito ai dati utilizzati ai fini di moderazione e prevenzione) e successivamente saranno cancellati in maniera sicura o anonimizzati in maniera irreversibile.

                                Ti rammentiamo infine che per le medesime finalità, i dati relativi al traffico telematico degli utenti attivi, esclusi comunque i contenuti delle comunicazioni, saranno conservati per un periodo non superiore ai 6 anni dalla data di comunicazione.        
                        </div>
                        </div>
                    </div>
                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Come assicuriamo la protezione dei tuoi dati?
                            </button>
                        </h2>
                        </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                        I dati sono raccolti dai soggetti indicati al punto 3, secondo le indicazioni della normativa di riferimento, con particolare riguardo alle misure di sicurezza previste dal GDPR (art. 32) per il loro trattamento mediante strumenti informatici, manuali ed automatizzati e con logiche strettamente correlate alle finalità indicate al punto 1 e comunque in modo da garantire la sicurezza e la riservatezza dei dati stessi.
                        Nel rispetto della normativa applicabile, è attivo un sistema di verifica antispam sulle comunicazioni tra utenti. I dati ivi inseriti potranno essere verificati al solo scopo di individuare attività illecite o contenuti non conformi alle Condizioni generali del Servizio, ma non saranno trattati o comunicati per finalità commerciali o promozionali.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            L'informativa sulla privacy può subire modifiche nel tempo?
                            </button>
                        </h2>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                        <div class="card-body">
                        La presente informativa potrebbe essere soggetta a modifiche. Qualora vengano apportate sostanziali modifiche all’utilizzo dei dati relativi all’utente da parte del Titolare, quest’ultimo avviserà l’utente pubblicandole con la massima evidenza sulle proprie pagine o tramite mezzi alternativi o similari.
                        </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 d-flex align-items-center">
                    <h1>Cookie</h1>   
                </div> 
                
                
                
           <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingSeven">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                    Cosa sono i cookie?
                    </button>
                </h2>
                </div>
                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                <div class="card-body">
                I cookie sono file di testo di dimensioni ridotte che i siti visitati dagli utenti inviano ai loro terminali o dispositivi, in cui vengono memorizzati per essere trasmessi nuovamente agli stessi siti in occasione di visite successive. I cookie (e/o tecnologie similari come ad esempio le tecnologie SDK per il mondo mobile) possono essere memorizzati in modo permanente (cookie persistenti) sul tuo dispositivo o avere un durata variabile; possono infatti cancellarsi con la chiusura del browser o avere una durata limitata alla singola sessione (cookie di sessione). I cookie possono essere installati da Presto (cookie di prima parte) o da altri siti web (cookie di terze parti).

                I cookie sono utilizzati per diverse finalità come meglio specificato al successivo punto 2 della presente pagina.

                Per le attività di profilazione, i dati personali raccolti tramite cookie sono trattati per un periodo massimo di 12 mesi dal momento in cui viene prestato il consenso al trattamento. Di seguito troverai tutte le informazioni sui cookie installati attraverso il sito di Subito e/o relative applicazioni e le indicazioni necessarie su come gestire le tue preferenze riguardo ad essi.

                Ti ricordiamo che la nostra piattaforma supporta i  browser e/o applicazioni di seguito individuati ai fini di una corretta erogazione del servizio.  Per risultati ottimali si consiglia di scaricare la versione più recente del proprio browser e/o app. 

                Ti ricordiamo che Subito non può garantire il corretto funzionamento del servizio e l’effettività delle indicazioni contenute nella presente informativa per precedenti versioni di browser e/o app non supportati.
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingEight">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                    Come viene prestato il consenso (opt-in) all'utilizzo dei cookie?
                    </button>
                </h2>
                </div>
                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
                <div class="card-body">
                Il consenso all’utilizzo dei cookie di profilazione viene prestato dall’utente attraverso le seguenti modalità: chiudendo il banner contenente l’informativa breve, scorrendo la pagina che ospita il banner o cliccando qualunque suo elemento e può essere revocato in ogni momento.
                Tutti i cookie tecnici non richiedono consenso, pertanto vengono installati automaticamente a seguito dell’accesso al sito o al servizio.
                </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingNine">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                    Come revocare (opt-out) il consensop all'utilizzo dei cookie?
                    </button>
                </h2>
                </div>
                <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                <div class="card-body">
                I cookie possono essere completamente disattivati dal browser utilizzando l’apposita funzione prevista nella maggior parte dei programmi di navigazione. È bene sapere però che disattivando i cookie alcune delle funzionalità di Subito potrebbero non essere utilizzabili. Riportiamo di seguito i link alle informative dei principali browser per ulteriori informazioni sulla disattivazione dei cookie: Chrome, Firefox, Internet Explorer, Safari, Edge. Per disattivare singolarmente i cookie di profilazione di terza parte clicca sui link all’informativa delle terze parti espressamente indicati al precedente punto 2 della presente policy sui cookie. Con riferimento ai cookie di profilazione finalizzati ad offrirti pubblicità personalizzata, ti informiamo che, qualora esercitassi l’opt-out, continuerai in ogni caso a ricevere pubblicità di tipo generico.Con riferimento ai cookie di profilazione marketing di prima e terza parte, è possibile disabilitarli in ogni momento anche mediante le impostazioni che trovi qui, tuttavia non godrai di una esperienza ottimale di navigazione.
                </div>
                </div>
            </div>
          </div>
   </div>
</div> 
</div>

@endsection

@extends('layouts.app')
@section('content')
<div class="container mx-3">

    <div class="row">
        <div class="col-12 pb-3">
            <h1 class="strong ml-0 pl-0">Chi siamo!</h1>

        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-5 align-items-center p-0">

            <img src="https://www.signorelli-partners.it/wp-content/uploads/2017/03/e-commerce-platforms.png" class="img-fluid pb-3" height="300" width="400" alt="origini di presto">

        </div>

        <div class="col-12 col-md-7">

            <div class="row">
                <h3 class="mt-0 pb-3">Le origini..</h3>

            </div>

            <div class="row">
                <p>Siamo la digital company n.1 in in Italia per comprare e vendere. Siamo nati a Roma nel 2020 e oggi, ci posizioniamo stabilmente tra i primi 10 brand online più visitati in Italia con 11 milioni di utenti ogni mese.
                    Da sempre il nostro obiettivo è offrire il servizio online di compravendita più semplice, veloce e sicuro d’Italia. Per questo, persone e utenti professionali scelgono ogni giorno Subito per fare ottimi affari. Dobbiamo i grandi risultati ottenuti ad una piattaforma intuitiva ed efficace e a un’offerta ampia e di qualità, con contenuti controllati per una maggiore sicurezza. E poi, certo, a una squadra instancabile fatta di persone che lavorano con passione per le persone.</p>
<br>

                    <p>* Ranking Total Digital Audience Daily di Audiweb Database</p>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="col-12 col-md-5 align-items-center p-0">

            <img src="https://www.filippo-ongaro.com/hubfs/4-passi-per-il-successo.jpg" class="img-fluid pb-3" height="300" width="400" alt="origini di presto">

        </div>

        <div class="col-12 col-md-7">

            <div class="row">
                <h3 class="mt-0 pb-3">8° posto nel 2019</h3>

            </div>

            <div class="row">
                <p>Grazie alla semplicità ed immediatezza dei nostri servizi, siamo riusciti a guadagnarci la fiducia dei nostri ustenti e clienti finali. Affidarci i vostri prodotti è garanzia di successo nostro e guadagno vostro!</p>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-5 align-items-center p-0">

            <img src="https://www.01net.it/wp-content/uploads/sites/14/2016/06/engagement_omini-1280x720.jpg" class="img-fluid pb-3" height="300" width="400" alt="origini di presto">

        </div>

        <div class="col-12 col-md-7">

            <div class="row">
                <h3 class="mt-0 pb-3">11 milioni di visitatori unici al mese</h3>

            </div>

            <div class="row">
                <p>Abbiamo raggiunto questi numeri solo grazie alla competenza del nostro team ed alla passione che ci mette nel migliorare la vostra experience sul nostro portale, ma il vero valore aggiunto siete voi!</p>
            </div>

        </div>

    </div>




</div>











@endsection
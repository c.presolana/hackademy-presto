@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-12">
            <h1>Il nostro utente:</h1>
            <div class="card mb-3" style="max-width: 1000px;">
              <div class="row no-gutters">
                <div class="col-md-8">
                  <div class="card-body">
                    <h5 class="card-title">{{$user->name}}</h5>
                    <h5 class="card-title">{{$user->email}}</h5>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-12">
        <h3>Articoli inseriti dall'utente:</h3>
        <ul>
        @foreach ($announcements as $announcement)
        <li>
          <a href="{{route('announcement.show', $announcement)}}">{{$announcement->title}}</a>
        </li>
        @endforeach
      </div>
    </div>
</div>
@endsection
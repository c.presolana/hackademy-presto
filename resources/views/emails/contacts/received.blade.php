<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row col-8">
            <h1>Buongiorno, hai ricevuto una nuova richiesta per questo prodotto </h1>
            <h2>Dati cliente:</h2>
            <ul>
            <li>Nome: {{$contact['name']}}</li>
            <li>Email: {{$contact['email']}}</li>
            <li>Telefono: {{$contact['phone']}}</li>
            </ul>
        <div class="col-8 mt-4">    
            <h3>{{$contact['message']}}</h3>
        </div>        
</div>
</body>
</html>
@extends('layouts.app')
@section('content')

    @if (Route::has('register'))

<div class=container>
    <div class=row>
        <div class="col-6 col-md-12">
            <h1>Il tuo profilo:</h1>
            <div class="card mb-3" style="max-width: 1000px;">
                <div class="row no-gutters">
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Nome: {{$user->name}}</h5>
                            <h5 class="card-title">E-mail: {{$user->email}}</h5>
                            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    

    @if (Auth::user()->is_revisor)
    <h1>ciao revisore</h1>
    @endif

    <div class="row">
        <div class="col-6 col-md-12">
            <h3>Articoli inseriti dall'utente:</h3>
            <ul>
                @foreach ($user->announcements as $announcement)
                    <li>{{$announcement->title}}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

@endif

@endsection
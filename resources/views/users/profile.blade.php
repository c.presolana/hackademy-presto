@extends('layouts.app')
@section('content')

    @if (Route::has('register'))

<div class=container>
    <div class=row>
        <div class="col-md-6 col-12">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-8">
                        <div class="card-body">
                            <h1 class="card-title">{{$user->name}}</h1>
                            <h4 class="card-title">{{$user->email}}</h4>
                            <p class="card-text"><small class="text-muted"></small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    


    <div class="row">
        <div class="col-md-6 col-12">
            <h3>Articoli inseriti dall'utente:</h3>
            <ul>
                @foreach ($user->announcements as $announcement)
                    @include('includes._announcement')
                @endforeach
            </ul>
        </div>
    </div>
</div>

@endif

@endsection
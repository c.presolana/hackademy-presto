@extends('layouts.app')
@section('content')
@if($announcement)
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('ui.announcement') }} : {{$announcement->id}}</div>
                <div class="card-body">
                    <div class="row">

                            <div class="col-md-2"><h3> {{ __('ui.users') }} </h3></div>

                            <div class="col-md-10">
                                # {{$announcement->user->id}},
                                {{$announcement->user->name}},
                                {{$announcement->user->email}}
                            </div>
                        </div>

                        <hr>
                        

                            <div class="row">
                            
                                <div class="col-md-2">
                                    <h3> {{ __('ui.title') }} </h3>
                                </div>

                                <div class="col-md-10">
                                    {{$announcement->title}}
                                </div>
                            </div>
                        
                        <hr>

                            <div class="row">

                                <div class="col-md-2">
                                    <h3> {{ __('ui.price') }} </h3>
                                </div>

                                <div class="col-md-10">
                                    {{$announcement->price}}
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-2">
                                    <h3>{{ __('ui.description') }}</h3>
                                </div>

                                <div class="col-md-10">
                                    {{$announcement->body}}
                                </div>
                            </div>

                        <hr>
                    
                        <div class="row">
                            <div class="col-md-2">
                            <h3> {{ __('ui.images') }} </h3>
                            </div>
                        

                            <div class="col-md-10">
                                @foreach($announcement->images as $image)
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <img src="{{ $image->getUrl(300,150) }}" alt="" class="rounded">
                                    </div>

                                    <div class="col-md-8">

                                        Adult:{{ $image->adult}}<br>
                                        Medical:{{ $image->medical}}<br>
                                        Spoof:{{ $image->spoof}} <br>
                                        Violence:{{ $image->violence}} <br>
                                        Racy:{{ $image->racy}} <br>
                                        <b>{{ __('ui.labels') }}</b><br>
                                
                                        @if($image->labels)
                                            @foreach(json_decode($image->labels) as $label)
                                                <h6 class="d-inline">{{$label}} ,</h6>
                                            @endforeach
                                        @endif
                                
                                    </div>
                                </div>
                                @endforeach
                            </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row justify-content-center mt-5">
        <div class="col-6 col-md-6">
            <form action="{{route('revisor.reject',$announcement->id)}}" method="POST">
                @csrf
                <button class="btn-lg btn-danger" type="submit">
                    {{ __('ui.refuse') }}
                </button>
            </form>
        </div>
        <div class="col-6 col-md-6">
            <form action="{{route('revisor.accept',$announcement->id)}}" method="POST">
                @csrf
                <button class="btn-lg btn-success" type="submit">
                {{ __('ui.accept') }}
                </button>
            </form>
        </div>
    </div>
</div>

    @else 
        <div class="container pt-5">
            <div class="row">
                <div class="col-12">
                    <h3>{{ __('ui.noAnnouncements') }}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
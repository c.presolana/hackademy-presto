@extends('layouts.app')
@section('content')


<!------ Include the above in your HEAD tag ---------->

  <!-- Contact Us Section -->
    <section class="section-dark">
      <div class="container">
          <div class="row">
              <!-- Section Titile -->
              <div class="col-md-6 mt-3 contact-widget-section2" data-wow-delay=".2s">
              <h1 class="section-title mb-4">{{ __('ui.revisorRequest') }}</h1>
                <p>{{ __('ui.requestDescription') }}</p>
              </div>
              <!-- contact form -->
              <div class="col-md-6 mt-4" data-wow-delay=".2s">
                  <form class="shake" role="form" method="post" id="contactForm" name="contact-form" data-toggle="validator">
                      <!-- Name -->
                      <div class="form-group label-floating">
                        <label class="control-label font-weight-bold" for="name">{{ __('ui.name') }}</label>
                        <input class="form-control" id="name" type="text" name="name" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>
                      <!-- email -->
                      <div class="form-group label-floating">
                        <label class="control-label font-weight-bold" for="email">Email</label>
                        <input class="form-control" id="email" type="email" name="email" required data-error="Please enter your Email">
                        <div class="help-block with-errors"></div>
                      </div>
                      <!-- Message -->
                      <div class="form-group label-floating">
                          <label for="message" class="control-label font-weight-bold">{{ __('ui.tellUsWhy') }}</label>
                          <textarea class="form-control" rows="3" id="message" name="message" required data-error="Write your message"></textarea>
                          <div class="help-block with-errors"></div>
                      </div>
                      <!-- Form Submit -->
                      <div class="form-submit mt-5">
                          <button class="btn btn-lg btn-confirm mt-2" type="submit" id="form-submit">{{ __('ui.sendRequest')}}</button>
                          <div id="msgSubmit" class="h3 text-center hidden"></div>
                          <div class="clearfix"></div>
                      </div>
                  </form>
              </div>
          </div>
      </div>
    </section>

@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('ui.verifyEmail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('ui.verificationLink') }}
                        </div>
                    @endif

                    {{ __('ui.emailVerificationLink') }}
                    {{ __('ui.ifEmailNotReceived') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('ui.clickToRequest') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

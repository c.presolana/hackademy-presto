<div class="col-12 col-md-4 p-4">
    <div class="card shadow">
        <a href="{{route('announcement.show', $announcement)}}">

            <div class="header col-12 pl-3 pt-4">
                <p>
                    @if($announcement->images)
                  <img src="{{ $announcement->images->first()->getUrl(300, 150) }}" alt="" class="rounded">
                    @endif
                 </p>
            </div>

            <div class="card-body">
                <h3>
                    {{$announcement->title}}
                </h3>
                <h4 style="color: #f9423a" class="card-text font-weight-bold pb-1">{{$announcement->price}} €</h4>
        </a>
        <p>
            {{$announcement->body}}
        </p>
        <h6>
        <a href="{{route('public.announcement.category', [
                    $announcement->category->name,
                    $announcement->category->id
                    ])}}"><strong class="presto-red">
                        {{$announcement->category->name}}</a>
                </strong>
        <i>{{$announcement->created_at->format('d/m/y')}} - {{$announcement->user->name}}</i>
        </h6>

    </div>

</div>
</div>
@guest

@if (Route::has('register'))

@endif

@else

@if (Auth::user()->is_revisor)

<!-- Revisor Navbar-->
<nav class="navbar sticky-top navbar-admin bg-dark navbar-expand-md">

    <div class="nav-item ml-md-5 mr-md-5 pl-4" href="#">
        <span style="color:white">
            {{ __('ui.revisorHome')}}
        </span></div>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
        <span style="color:white">
            <i class="fas fa-caret-down"></i>
        </span>
    </button>

        

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="basicExampleNav">
        <!-- Links -->
        <ul class="navbar-nav mr-auto">
            
            <li class="nav-item ">
                <a href="{{route('revisor.home')}}" class="nav-link ">
                    <span class="nav-revisor-item">
                        <i class="fas fa-edit pr-md-2"></i>
                        {{\App\Announcement::ToBeRevisionedCount()}} {{ __('ui.announcementrevision')}}
                    </span>

                </a>
            </li>

           
        </ul>
    </div>

    <!-- Collapsible content -->
</nav>

<!--/.Navbar-->

@endif
@endguest


<!-- Navbar -->
<nav class="navbar pl-md-5 pt-4 pb-4 navbar-expand-md navbar-light bg-white shadow-sm">

    <!-- Navbar brand -->
    <a class="navbar-brand ml-md-5" href="{{ url('/') }}"><img src="/media/presto.png" alt="logo presto"></a>

    <!-- Collapse button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent2">

        <!-- Left Side Of Navbar -->

        <ul class="navbar-nav mt-md-2 ml-md-3 mr-auto">
            <li class="nav-item dropdown">
                <h5>
                    <a v-prev aria-expanded="false" aria-haspopup="true" data-toggle="dropdown" role="button" id="categoriesDropdown" href="" class="nav-link ">
                     {{ __('ui.allCategories') }} <span class="caret"></span></a>
                    <ul aria-label="categoriesDropdown" class="p-3 dropdown-menu dropdown-menu-right">
                        @foreach ($categories as $category)
                        <a href="{{ route('public.announcement.category', [$category->name, $category->id]) }}" class="py-2 nav-link text-uppercase font-weight-light"> {{ $category->name }} </a>
                        @endforeach
                    </ul>
                </h5>
            </li>
        </ul>


        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto mt-md-2">
            <!-- Authentication Links -->

            <li class="nav-item mr-md-3">
            @include('includes._locale',['lang'=>'en','nazione'=>'gb'])
            </li>
            <li class="nav-item mr-md-3">
            @include('includes._locale',['lang'=>'it','nazione'=>'it'])
            </li>
               
                @guest

                <li class="nav-item mr-md-3">
                <h5><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></h5>
                </li>

                @if (Route::has('register'))
                <li class="nav-item mr-md-3">
                <h5><a class="nav-link" href="{{ route('register')}}">{{ __('ui.register') }}</a></h5>
                </li>
                @endif

                @else

                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>{{ __('ui.hello') }},
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="py-1 px-4 dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                        
                    </div>
                </li>
                @endguest

            </h5>

            <li class="ml-md-3 nav-item pr-md-5">
                <h5>
                    <a class="p-md-2 nav-link btn-announcement" href="{{ route('announcement.new') }}"><i class="far fa-announcement fa-plus-square mr-md-1"></i>
                    {{ __('ui.createAnnouncement') }}</a>
                </h5>
            </li>

        </ul>

    </div>


    <!-- Collapsible content -->

</nav>
<!-- Navbar -->
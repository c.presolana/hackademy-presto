<!-- Footer -->
<footer class="page-footer bg-light font-small blue">

  <!-- Footer Links -->
  <div class="container text-center text-md-left">

    <!-- Grid row -->
    <div class="row pt-5 footer-border">

      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h6 class="font-weight-bold text-black">Presto rules</h6>

        <ul class="list-unstyled">
          <li>
            <a class="text-secondary" href="{{route('announcements.privacy')}}">Privacy</a>
          </li>
          <li>
            <a class="text-secondary" href="{{route('announcements.condizioni')}}">{{ __('ui.conditions') }}</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->




      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h6 class="font-weight-bold text-black">Info</h6>

        <ul class="list-unstyled">


          <li>
            <a class="text-secondary" href="{{ route('revisor.formRequest') }}">{{ __('ui.work') }}</a>
          </li>
          

          <li>
            <a class="text-secondary" href="#!">{{ __('ui.aboutUs') }}</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-6 mb-md-0 mb-3">

        <!-- Links -->
        <h6 class="font-weight-bold text-black">{{ __('ui.followus')}}</h6>

        <ul class="list-unstyled mt-2">
          <span style="color: #4267B2;">
            <i class="fab fa-facebook-square fa-2x mr-3"></i>
          </span>
          <span style="color: black;">
            <i class="fab fab fa-instagram fa-2x mr-3"></i>
          </span>
          <span style="color: red;">
            <i class="fab fa-youtube fa-2x mr-3"></i>
          </span>
          <span style="color: #1DA1F2;">
            <i class="fab fa-twitter fa-2x mr-3"></i>
          </span>
        </ul>

      </div>
      <!-- Grid column -->
     

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-secondary text-center py-3 pt-5"> © 2020 Copyright:
    Presto  Via della Liberazione, CAP 0100, Roma.   P.IVA 84038724187 
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PublicController

Route::get('/annunci/dettaglio/{announcement}', 'PublicController@show')->name('announcement.show');
Route::get('/privacy', 'PublicController@privacy')->name('announcements.privacy');
Route::get('/condizioni', 'PublicController@condizioni')->name('announcements.condizioni');
Route::get('/aboutus', 'PublicController@aboutus')->name('announcements.aboutus');
Route::get('/utente/dettaglio/{user}','PublicController@usershow')->name('announcements.usershow');
Route::get('/announcement/form','PublicController@create')->name('announcements.create');
Route::post('/announcement/inviato','PublicController@store')->name('announcements.store');
Route::get('/announcement/grazie','PublicController@grazie')->name('announcements.grazie');
Route::post('/locale/{locale}','PublicController@locale')->name('locale');
Route::get('/', 'PublicController@index');
Route::get('/category/{name}/{id}/announcements', 'PublicController@announcementsByCategory')->name('public.announcement.category'); 
Route::get('/ricerca/risultati', 'PublicController@search')->name('search');
// HomeController

Route::get('/annunci/nuovo', 'HomeController@newAnnouncement')->name('announcement.new');
Route::post('/annunci/creazione', 'HomeController@createAnnouncement')->name('announcement.create');
Route::post('/announcement/images/upload','HomeController@uploadImages')->name('announcement.images.upload');
Route::delete('/announcement/images/remove','HomeController@removeImages')->name('announcement.images.remove');
Route::get('/announcement/images', 'HomeController@getImages');
Route::get('/revisor/request', 'HomeController@formRequest')->name('revisor.formRequest');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Revisor Controller

Route::get('/revisor/home', 'RevisorController@index')->name('revisor.home');
Route::post('/revisor/annunci/{id}/rifiutato', 'RevisorController@reject')->name('revisor.reject');
Route::get('/revisor/annunci/rifiutati', 'RevisorController@rejectedAnnouncements')->name('revisor.rejectedAnnouncements');
Route::post('/revisor/annunci/{id}/accettato', 'RevisorController@accept')->name('revisor.accept');


Route::get('/user/profilo', 'UserController@profile')->name('user.profile');

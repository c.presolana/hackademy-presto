<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Jobs\ResizeImage;
use App\AnnouncementImage;
use Illuminate\Http\Request;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function newAnnouncement(Request $request) 
    {
        $uniqueSecret = $request->old('uniqueSecret',base_convert(sha1(uniqid(mt_rand())),16,36));
        return view('announcements.new',compact('uniqueSecret'));
    }

    public function createAnnouncement(AnnouncementRequest $request)
    {

        $c = new Announcement();

        $c->title = $request->input('title');
        $c->price = $request->input('price');
        $c->category_id = $request->input('category_id');
        $c->body = $request->input('body');
        $c->user_id = Auth::id();

        $c->save();

        $uniqueSecret = $request->input('uniqueSecret');
        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedimages = session()->get("removedimages.{$uniqueSecret}",[]);
   
        $images = array_diff($images, $removedimages);
        
        foreach ($images as $image){
            $i = new AnnouncementImage();
            
            $fileName = basename($image);
            $newFileName ="public/announcements/{$c->id}/{$fileName}";
            Storage::move($image,$newFileName);

            dispatch (new ResizeImage(
                 $newFileName,
                 600,
                 300
                 ));

             dispatch (new ResizeImage(
                 $newFileName,
                 400,
                 300
                 ));

             dispatch (new ResizeImage(
                 $newFileName,
                 300,
                 150
                 ));

             dispatch (new ResizeImage(
                 $newFileName,
                 120,
                 120
                 ));
            
            $i->file = $newFileName;
            $i->announcement_id = $c->id;
            $i->save();
            
            
            dispatch(new GoogleVisionSafeSearchImage($i->id));
            dispatch(new GoogleVisionLabelImage($i->id));

            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new ResizeImage( $newFileName,300,150),
                new ResizeImage( $newFileName,400,300),
                new ResizeImage( $newFileName,600,300),
                new ResizeImage( $newFileName,120,120)
                ])->dispatch($i->id);
            
         }
           
            File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

            return redirect('/')->with('announcement.created.success','ok');
         

    }


    public function uploadImages(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch (new ResizeImage(
            $fileName,
            120,
            120
            ));

        session()->push("images.{$uniqueSecret}",$fileName);

        return response()->json(
            [
                'id' => $fileName
            ]
            );
    
    }
    public function removeImages(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}",$fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }

    public function getImages(Request $request){
        
        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}",[]);
        $removedimages = session()->get("removedimages.{$uniqueSecret}",[]);
        
        $images = array_diff($images, $removedimages);
        
        
        $data = [];
        
        foreach ($images as $image) {
            
            $data[] =[
                'id' => $image,
                'src' => AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }
        
        return response()->json($data);
    }

    
    public function formRequest(){
        return view('revisor.formRequest');
    }

}

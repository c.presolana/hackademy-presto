<?php

namespace App\Http\Controllers;

use App\User;
use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RevisorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.revisor');
    }

    public function index()
    {

        $announcement = Announcement::where('is_accepted', null)
        ->orderBy('created_at','desc')
        ->first();

        //dd(json_decode($announcement->images->first()->labels));
        return view('revisor.home', compact('announcement'));
    }
    private function setAccepted($announcement_id,$value){

        $announcement = Announcement::find($announcement_id);
        $announcement->is_accepted=$value;
        $announcement->save();

        return redirect(route('revisor.home'));

    }

    public function accept($announcement_id){

        return $this->setAccepted($announcement_id, true);

    }
    public function reject($announcement_id){

        return $this->setAccepted($announcement_id, false);
    }

    public function rejectedAnnouncements(User $user){

        $user = Auth::user();
        $announcement = Announcement::where('is_accepted', false)
        ->orderBy('created_at', 'desc')
        ->first();
        return view ('revisor.rejected', compact('announcement', 'user'));
    }

}

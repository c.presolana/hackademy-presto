<?php

namespace App\Http\Controllers;

use App\User;
use App\Category;
use App\Announcement;
use Illuminate\Http\Request;
use App\Mail\ContactReceived;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function index()
    {
        $announcements = Announcement::where('is_accepted',true)
        ->orderBy('created_at', 'desc')
        ->paginate(9);
        return view ('welcome', compact('announcements'));
    }

    public function show(Announcement $announcement)
    {
        $announcements = Announcement::where('category_id', $announcement->category_id)
        ->paginate(3);
        $users = User::all();
        return view ('announcements.show', compact('announcement','announcements','users'));
    }

    public function announcementsByCategory($name, $category_id)
    {
        $category = Category::find($category_id);
        $announcements = $category->announcements()
        ->where('is_accepted',true)
        ->orderBy('created_at', 'desc')
        ->paginate(9);
        return view ('announcements.announcements', compact('category', 'announcements'));
    }

    public function search(Request $request){
        $q=$request->input ('q');
        $announcements=Announcement::search($q)->where('is_accepted',true)->get();
        return view('announcements.search',compact('q','announcements'));
    }

    public function locale($locale){
        
        session()->put('locale',$locale);

       return redirect()->back();
    }


    public function create()
    {
        return view('announcements.create');
    }

    public function usershow(User $user)
    {
        $announcements = $user->announcements;
        return view ('announcements.usershow', compact('user','announcements'));
    }



    public function store(Request $request)
    {
          $name = $request->input('name');
          $phone = $request->input('phone');
          $email = $request->input('email');
          $message = $request->input('message');
          $contact = compact('name','phone','email','message');


          Mail::to('lcl@gmail.com')->send(new ContactReceived($contact));


          return redirect(route('announcements.grazie'));
        
    }
    
    public function grazie(){
        return view('announcements.grazie');
    }

    public function privacy(){

        return view('announcements.privacy');
    }

    public function condizioni(){

        return view('announcements.condizioni');
    }

    public function aboutus(){

        return view('announcements.aboutus');
    }

}
       
 



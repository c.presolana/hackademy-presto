<?php

namespace App;

use App\User;
use App\Category;
use App\AnnouncementImage;
use App\AnnouncementImages;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{

  use Searchable;

  public function toSearchableArray()
  {      
      $array = [
        'id' => $this->id,
        'title' => $this->title,
        'body' => $this->body,
        'categorie' => $this->category->name,
      ];

      return $array;
  }

  public function category()
  {
    return $this->belongsTo(Category::class);
  }

  public function user()
  {

    return $this->belongsTo(User::class);
  }

  public function images(){

    return $this->hasMany(AnnouncementImage::class);
   }

  static public function ToBeRevisionedCount()
  {
    return Announcement::where('is_accepted', null)->count();
  }

  static public function RejectedCount()
  {
    return Announcement::where('is_accepted', false)->count();
  }
}
